# Godot Inheritance Problems

I'm struggling to figure out how members and setters/getters work with inheritance.

> The example is extremely simplified from my usecase.

I have a class `AbstractFightingCharacter` (that is suposed to be an abstract class).
It has a series of members and getters/setters.

Then I created a subclass `MonsterFightingCharacter`.

(the purpose of this architecture is to use composition to have the fields' values come from different data classes / types)

I want to be able to set the `current_hp` of my instance in the constructor, after some logic.  
It's not working!

If you run the scene and examine the `Scene.gd` script you can probably see what I want to do.

I believe I'm not grokking how inheritance and member lookup works in GD Script :(

# Solution Found

It appears that:

1. when using `self`, the setter/getter is called
2. when it's not used, it uses the field as it is _in the file_. 

From (2), I should use `self.field` when referencing fields in the super-class so that the sub-class' getter is used.

From the documentation: 

> local access will not trigger the setter and getter.

(check the example)
https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_basics.html?highlight=super#setters-getters


Here's a proposed fix, and validation of the above:
```
# AbstractFightingCharacter.gd

func _set_current_hp(value: int) -> void:
    print("I'm in the baseclass setter")
    current_hp = int(max(0, min(value, self.total_hp)))

# MonsterFightingCharacter

func _init(_current_hp: int = -1):
    print("calls the setter:")
    self.current_hp = 222
    print("does not call the setter:")
    current_hp = 777
```

Result:
```
calls the setter:
I'm in the baseclass setter
does not call the setter:
```
