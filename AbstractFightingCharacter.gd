
# Not to be used. This is an Abstract class
class_name AbstractFightingCharacter

var total_hp: int setget _set_total_hp,_get_total_hp
var current_hp: int setget _set_current_hp

func _init():
    self.total_hp = 10000

func _set_total_hp(_value: int) -> void:
    push_error("You shouldn't be doing this!")


func _get_total_hp() -> int:
    push_error("This should be overridden")
    return 0


func _set_current_hp(value: int) -> void:
    print("In Base: value = %s" % value)
    current_hp = int(max(0, min(value, total_hp)))
    print("In Base: total_hp = %s" % total_hp)
    print("In Base: current_hp = %s" % current_hp)
    print("In Base: self.total_hp = %s" % self.total_hp)
    print("In Base: self.current_hp = %s" % self.current_hp)
    emit_signal("character_health_changed", current_hp, total_hp)
